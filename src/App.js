import avatar from "./assets/images/48.jpg";
import "bootstrap/dist/css/bootstrap.min.css";

const style = {
  container: {
    width: "700px",
    marginTop: "0 auto",
    textAlign: "center",
    marginTop: "100px",
    border: "1px solid red",
    padding: "30px",
  },
  avatar: {
    borderRadius: "50%",
    marginTop: "85px",
    width: "100px",
    marginBottom: "30px",
  },
  quote: {
    fontSize: "17px",
    marginBottom: "20px",
    opacity: "0.8",
  },
  name: {
    fontWeight: "bold",
  }
}
function App() {
  return (
    <div style={style.container} className="container">
      <div>
        <img style={style.avatar} src={avatar} alt="avatar" />
      </div>
      <div style={style.quote}>
        This is one of the best developer blogs on the planet! i read it daily to improve my skills.
      </div>
      <div>
        <span style={style.name}>
          Tammy Stevens &nbsp;
        </span>
        <span>
          . &nbsp;Front End Developer
        </span>
      </div>
    </div>
  );
}

export default App;
